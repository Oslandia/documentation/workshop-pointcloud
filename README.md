English
-------

This is a workshop on PointCloud data manipulation with :
- PDAL
- PostGIS
- PgPointCloud
- QGIS

The workshop is located in the [en directory](./en) for the english version.

Français
--------

Ceci est un workshop sur le traitement de données de nuages de points avec : 
- PDAL
- PostGIS
- PgPointCloud
- QGIS

La version française est dans le [répertoire fr](./fr)


Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
